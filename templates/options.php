<div class="tlab_settings">
        <div class="col-sm-12">

            <div class="breadcrumb">
                <h1><i class="fa fa-calendar"></i> Agenda</h1>
                <p>Vous trouverez dans cette page tous les outils nécessaires à la gestion de votre agenda</p>
            </div>

            <ul class="nav nav-pills navbar-default navbar">
                <li class="active"><a data-toggle="pill" href="#dashboard"><i class="fa fa-home"></i> Accueil</a></li>
                <li><a data-toggle="pill" href="#settings"><i class="fa fa-cog"></i> Administration</a></li>
            </ul>

            <div class="tab-content">

                <!-- Accueil -->
                <div id="dashboard" class="tab-pane fade in active">
                    <div class="panel-group">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="panel panel-default event-listing">

                                    <!--  Actualités nationales privées  -->
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h2><i class="fa fa-calendar"></i> Évènements</h2>
                                            </div>
                                            <div class="col-xs-8">
                                                <!-- Button trigger modal -->
                                                <div class="nav  navbar-right">
                                                        <div class="col-xs-6">
                                                            <button type="button" title="Ajouter un évènement" class="btn btn-primary txt_right" data-toggle="modal" data-target="#createEvent">
                                                                <i class="fa fa-plus" aria-hidden="true"></i> Ajouter un évènement
                                                            </button>
                                                        </div>
                                                </div>

                                            </div>
                                        </div>

                                        <?php if($this->user->is_DSC()) : ?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="filters">
                                                    <div class="row">
                                                        <div class="col-xs-2"><p><i class="fa fa-filter"></i> Filtrer</p></div>
                                                        <div class="col-xs-10">
                                                            <form action="">
                                                                <div class="row">
                                                                    <div class="col-xs-6">
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="form-group">
                                                                            <?php echo do_shortcode('[regions_as_filter]'); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                    </div>

                                    <div class="panel-body">
                                        <div class="row">
                                                <?php echo do_shortcode('[index_events]') ?>
                                        </div>
                                    </div>



                                    <!-- Modal -->
                                    <div class="modal fade" id="createEvent" tabindex="-1" role="dialog" aria-labelledby="createEvent" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <h2 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> Ajouter un évènement</h2>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-body">
                                                    <?php echo do_shortcode('[create_event]'); ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modifier un évènement -->
                                    <div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="editEvent" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <h2 class="modal-title"><i class="fa fa-plus"></i> Modifier un évènement</h2>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-body edit_event_container">
                                                    <?php echo do_shortcode('[edit_event]'); ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                            <div class="col-sm-6">
                                <div class="panel panel-default">

                                    <!--  Actualités nationales publiques  -->
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <h2><i class="fa fa-calendar"></i> Agenda</h2>
                                            </div>
                                            <div class="col-xs-3">
                                                <!-- Button trigger modal -->
                                                <div class="nav  navbar-right">


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <?php echo do_shortcode('[display_calendar]') ?>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <?php if($this->user->is_DS() || $this->user->is_DSC() || $this->user->is_ADMIN()) : ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Les évènement en attentes de publication dans votre région</div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <?php echo do_shortcode('[pending_index]'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php endif; ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">Mes brouillons</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <?php echo do_shortcode('[draft_index]'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>


                        </div>
                    </div>

                </div>

                <div id="settings" class="tab-pane fade">
                    <div class="panel-group">
                        <div class="row">






                            <?php if($this->user->is_DSC() || $this->user->is_ADMIN()) : ?>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">

                                        <!--  Régions  -->
                                        <div class="panel-heading">
                                            <h2>Régions</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-xs-12">

                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#indexRegion">
                                                        Afficher la liste des régions
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="indexRegion" tabindex="-1" role="dialog" aria-labelledby="indexRegion" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <div class="row">
                                                                        <div class="col-xs-9">
                                                                            <h3 class="modal-title" id="exampleModalLongTitle">
                                                                                Toutes les régions
                                                                            </h3>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <?php echo do_shortcode('[index_regions]'); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createRegion">
                                                        Ajouter une région
                                                    </button>

                                                    <!-- ADD REGION - Modal -->
                                                    <div class="modal fade" id="createRegion" tabindex="-1" role="dialog" aria-labelledby="createRegion" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <div class="row">
                                                                        <div class="col-xs-9">
                                                                            <h3 class="modal-title" id="exampleModalLongTitle">
                                                                                Ajouter une région
                                                                            </h3>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <?php echo do_shortcode('[create_region]'); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-xs-12">
                                                        <div class="btn-group btn-group-vertical">
                                                            <button class="btn btn-primary btn-block" onclick="javascript: generateRegions()">Générer toutes les régions</button>
                                                            <button class="btn btn-danger btn-block" onclick="javascript: removeRegions()">Supprimer les régions</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">Rôles</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="btn-group btn-group-vertical">
                                                        <button class="btn btn-primary btn-block" onclick="javascript: generateRoles()">Générer les rôles utilisateurs</button>
                                                        <button class="btn btn-danger btn-block" onclick="javascript: removeRoles()">Supprimer les rôles utilisateurs</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div></div>
                    </div>
                </div>
            </div>

        </div>
</div>