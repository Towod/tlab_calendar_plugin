<div class="col-xs-12">
    <article class="event" id="tlab-event-<?php echo $event_datas['id']; ?>">

        <div class="row">
            <div class="col-sm-3">
                <?php echo $event_datas['thumbnail']; ?>
            </div>
            <div class="col-sm-9">
                <div class="content">
                    <div class="row">


                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-10">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <header>
                                                    <h3><?php echo $event_datas['title']; ?></h3>

                                                </header>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="event-date">
                                                    <h4><i class="fa fa-calendar"></i> Début</h4>
                                                    <div>
                                                        <p><?php echo $event_datas['start_date']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="event-time">
                                                    <h4><i class="fa fa-calendar"></i> Fin</h4>
                                                    <div>
                                                        <p><?php echo $event_datas['end_date']; ?></p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </div>

                                <div class="col-xs-2">
                                    <div class="btn-group btn-group-vertical">



                                        <a class=" btn btn-primary" title="Voir l'événement" href="<?php echo $event_datas['permalink']; ?>" target="_blank"><i class="fa fa-eye"></i></a>

                                        <?php if($event_datas['status'] == 'pending' || $event_datas['status'] == 'draft'  ) : ?>

                                            <?php if($this->user->is_DS() || $this->user->is_ADMIN() ) : ?>
                                                <button class="validate_event btn btn-success" title="Valider l'évènement" id="<?php echo 'btn-validate-event-'.$event_datas['id']; ?>" ><i class="fa fa-check"></i></button>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if($event_datas['status'] == 'draft') : ?>
                                            <?php if(!$this->user->is_DSC() && !$this->user->is_DS() && !$this->user->is_ADMIN())  : ?>
                                                <button class="edit_event btn btn-warning" title="Modifier" id="<?php echo 'btn-edit-event-'.$event_datas['id']; ?>" data-toggle="modal" data-target="#editEvent"><i class="fa fa-pencil"></i></button>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if($this->user->is_DSC() || $this->user->is_DS() || $this->user->is_ADMIN())  : ?>
                                        <button class="edit_event btn btn-warning" title="Modifier" id="<?php echo 'btn-edit-event-'.$event_datas['id']; ?>" data-toggle="modal" data-target="#editEvent"><i class="fa fa-pencil"></i></button>

                                        <button class="destroy_event btn btn-danger" title="Supprimer" ><i class="fa fa-trash"></i></button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>



    </article>
</div>