<form class="create-event create-event-form" action="">
    <div class="row">

        <div class="col-sm-9">
            <div class="form-group">
                <label for="event_name">Nom de l'évènement</label>
                <input id="event_name" name="event_name" class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label for="event_description">Description de l'évènement</label>
                <?php wp_editor( '', 'event_description', array( 'quicktags' => FALSE ) ); ?>
            </div>


            <?php if($this->user->is_DSC() || $this->user->is_ADMIN()) : ?>
            <div class="form-group">
                <label for="event_description">Région dans laquelle poster l'évènement</label>
                <?php echo do_shortcode('[regions_as_select]'); ?>
            </div>
            <?php else : ?>
                <input id="tlab_region_id" name="tlab_region_id" type="hidden" value="<?php echo $this->user->getRegionID() ?>" />
            <?php endif; ?>

            <div class=" form-group">
                <label><i class="fa  fa-image"></i> Image de couverture</label>

                <div class=" panel panel-default">

                    <div class='image-preview-wrapper'>
                        <img id='image-preview' src='' width='100' height='100' style='max-height: 100px; width: 100px;'>
                    </div>
                    <input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
                    <input type='hidden' name='image_attachment_id' id='image_attachment_id' value=''>
                </div>
            </div>


            <div class="form-group">
                <div class="response alert-info"><p></p></div>
            </div>
        </div>

        <div class="col-sm-3">

            <div class="row">

                <?php wp_nonce_field( 'create_event', 'create_event_nonce' ); ?>

                <div class="form-group picker col-xs-12">
                    <label><i class="fa  fa-calendar"></i> Date de début</label>

                    <div class="input input-group">
                        <input type="" name="tlab_events_startdate" id="tlab_events_startdate" class="tlabdate form-control" value="<?php echo date('j/m/Y'); ?>" disabled="disabled" />
                    </div>
                </div>
                <div class="form-group picker col-xs-12">
                    <label><i class="fa  fa-calendar"></i> Date de fin</label>
                    <div class="input"><input name="tlab_events_enddate" id="tlab_events_enddate" class="tlabdate form-control" value="<?php echo date('j/m/Y'); ?>" disabled="disabled"/></div>

                </div>

                <div class="form-group picker col-xs-12 start_time">
                    <label><i class="fa  fa-clock-o"></i> Heure d'ouverture</label>

                    <div class="tlab_timepicker timepicker-wrapper">
                        <select class="timepicker form-control timepicker_hour ">
                            <?php for( $i=0 ; $i < 24 ; $i++) :

                                    $value = ($i<10) ? '0'.$i : $i;
                            ?>
                                    <option value="<?php echo $value; ?>" <?php if($i == 9) echo 'selected'; ?>><?php echo $value; ?></option>
                            <?php endfor; ?>
                        </select> :
                        <select class="timepicker form-control timepicker_minutes">
                            <?php for( $i=0 ; $i < 60 ; $i+=5) :
                                $value = ($i<10) ? '0'.$i : $i;
                            ?>
                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>

                    <div class="input input-group"><input type="hidden" name="tlab_events_starttime"  id="tlab_events_starttime" value="09:00" class="form-control" /></div>
                </div>

                <div class="form-group col-xs-12 end_time">
                    <label><i class="fa  fa-clock-o"></i> Heure de fermeture</label>

                    <div class="tlab_timepicker timepicker-wrapper">
                        <select class="timepicker form-control timepicker_hour">
                            <?php for( $i=0 ; $i < 24 ; $i++) :

                                $value = ($i<10) ? '0'.$i : $i;
                                ?>
                                <option value="<?php echo $value; ?>" <?php if($i == 10) echo 'selected'; ?>><?php echo $value; ?></option>
                            <?php endfor; ?>
                        </select> :
                        <select class="timepicker form-control timepicker_minutes">
                            <?php for( $i=0 ; $i < 60 ; $i+=5) :
                                $value = ($i<10) ? '0'.$i : $i;
                                ?>
                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>

                    <input name="tlab_events_endtime" id="tlab_events_endtime" type="hidden" value="10:00" class="form-control" />
                </div>



                <div class="col-sm-12 form-group">
                    <button class="btn btn-block btn-warning store_rush btn-block" >Enregistrer le brouillon</button>


                    <?php if($this->user->is_DSC() || $this->user->is_DS() || $this->user->is_ADMIN()) : ?>
                        <input class="btn btn-block btn-primary" type="submit" value="Ajouter l'évènement">
                    <?php endif; ?>

                    <?php if($this->user->is_ELU() || $this->user->is_SYN() || $this->user->is_ADMIN()) : ?>
                        <button class="btn btn-block btn-primary store_to_validate btn-block" >Proposer l'événement</button>
                    <?php endif; ?>

                </div>

            </div>
        </div>


    </div>

</form>