
    <article class="event" id="tlab-event-<?php echo $event_datas['id']; ?>">

        <div class="row">
            <div class="col-sm-3">
                <?php echo $event_datas['thumbnail']; ?>
            </div>
            <div class="col-sm-9">
                <div class="content">
                    <div class="row">


                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <header>
                                                    <h3><?php echo $event_datas['title']; ?></h3>
                                                    <p><?php echo $event_datas['excerpt']; ?></p>
                                                </header>
                                            </div>
                                            <div class="col-xs-6">

                                            </div>
                                            <div class="col-xs-6">


                                            </div>
                                        </div>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                        <div class="event-date">
                                            <h4><i class="fa fa-calendar"></i> Début</h4>
                                            <div>
                                                <p><?php echo $event_datas['start_date']; ?></p>
                                            </div>
                                        </div>
                                        <div class="event-time">
                                            <h4><i class="fa fa-calendar"></i> Fin</h4>
                                            <div>
                                                <p><?php echo $event_datas['end_date']; ?></p>
                                            </div>
                                        </div>

                                        <a class=" btn btn-primary btn-block" title="En savoir plus sur cet événement" href="<?php echo $event_datas['permalink']; ?>"><i class="fa fa-plus"></i> En savoir plus</a>

                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>



    </article>