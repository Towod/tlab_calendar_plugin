<form class="create-region" action="" id="createRegionForm">
        <div class="col-sm-12">
            <div class="response alert-info"><p></p></div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="region_name">Nom de la région</label>
                <input id="region_name" name="region_name" class="form-control" type="text" />
            </div>
            <div class="form-group">
                <input class="btn btn-block btn-primary" type="submit" value="Ajouter la région">
            </div>

        </div>

</form>