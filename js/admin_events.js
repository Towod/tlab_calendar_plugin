jQuery(document).ready(function($)
{
	$(".tlabdate").datepicker({
	    dateFormat: 'dd/mm/yy',
	    showOn: 'button',
        buttonText: '<i class="fa fa-calendar"></i>',
	    buttonImageOnly: false,
	    numberOfMonths: 1
	 
	}).next(".ui-datepicker-trigger").addClass("btn btn-primary").next();

	$(".tlabtime").datepicker({
        timeFormat:  "hh:mm",
	    showOn: 'button',
        buttonText: '<i class="fa fa-bell"></i>',
	    buttonImageOnly: false,

	}).next(".ui-datepicker-trigger").addClass("btn btn-primary").next();

    $('#createRegion').on('shown.bs.modal', function () {
        $(this).find('input').first().focus();
    });

    $('#indexRegion').on('shown.bs.modal', function () {
    });

    $('#createEvent').on('shown.bs.modal', function () {
        $(this).find('input').first().focus();
    });

    $('#createRegionForm').on('submit', function(e) {
        e.preventDefault();

        jQuery.post(
            ajaxurl,
            {
                'action': 'store_region',
                'region_name': $(this).find('input[name="region_name"]').val(),
            },
            function(response){
            	$('form.create-region').find('.response p').text(response);
            }
        );
    });

    // Supprimer un évènement
    $('.destroy_event').live('click', function(e) {
        e.preventDefault();


        jQuery.post(
            ajaxurl,
            {
                'action': 'destroy_event',
                'event_id': $(this).closest('article.event').attr('id').replace('tlab-event-', ''),
            },
            function(response){
            	console.log(response);

                window.location.reload();
            }
        );
    });

    var tlab_thumbnail;

    $('input[type="file"]').on('change', function(event) {
        tlab_thumbnail = event.target.file;

        console.log(tlab_thumbnail);
    });

    $('.create-event-form').on('submit', function(e) {
        e.preventDefault();

        var description = tmce_getContent('event_description', 'event_description');
        $.ajax({
            url                     : ajaxurl,
            type                    : 'POST',
            data                    : {
                action:             'store_event',
                event_name:         $(this).find('#event_name').val(),
                event_description:  description,
                event_region_id:    $(this).find('#tlab_region_id').val(),
                event_start_date:   $(this).find('#tlab_events_startdate').val(),
                event_end_date:     $(this).find('#tlab_events_enddate').val(),
                event_start_time:   $(this).find('#tlab_events_starttime').val(),
                event_end_time:     $(this).find('#tlab_events_endtime').val(),
                thumbnail:          $(this).find('#image_attachment_id').val()
            },

            success: function (response, statut) {
                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
                window.location.reload();
            },
            error: function(response, statut, erreur) {

                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
            },
            complete: function(resultat, statut) {

            }
        });
    });

    $('.store_to_validate').on('click', function(e) {
        e.preventDefault();

        var description = tmce_getContent('event_description', 'event_description');
        $.ajax({
            url                     : ajaxurl,
            type                    : 'POST',
            data                    : {
                action:             'store_to_validate',
                event_name:         $(this).closest('form').find('#event_name').val(),
                event_description:  description,
                event_region_id:    $(this).closest('form').find('#tlab_region_id').val(),
                event_start_date:   $(this).closest('form').find('#tlab_events_startdate').val(),
                event_end_date:     $(this).closest('form').find('#tlab_events_enddate').val(),
                event_start_time:   $(this).closest('form').find('#tlab_events_starttime').val(),
                event_end_time:     $(this).closest('form').find('#tlab_events_endtime').val(),
                thumbnail:          $(this).closest('form').find('#image_attachment_id').val()
            },

            success: function (response, statut) {
                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
                window.location.reload();
            },
            error: function(response, statut, erreur) {

                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
            },
            complete: function(resultat, statut) {

            }
        });
    });

    $('.store_rush').on('click', function(e) {
        e.preventDefault();

        var description = tmce_getContent('event_description', 'event_description');
        $.ajax({
            url                     : ajaxurl,
            type                    : 'POST',
            data                    : {
                action:             'store_rush',
                event_name:         $(this).closest('form').find('#event_name').val(),
                event_description:  description,
                event_region_id:    $(this).closest('form').find('#tlab_region_id').val(),
                event_start_date:   $(this).closest('form').find('#tlab_events_startdate').val(),
                event_end_date:     $(this).closest('form').find('#tlab_events_enddate').val(),
                event_start_time:   $(this).closest('form').find('#tlab_events_starttime').val(),
                event_end_time:     $(this).closest('form').find('#tlab_events_endtime').val(),
                thumbnail:          $(this).closest('form').find('#image_attachment_id').val()
            },

            success: function (response, statut) {
                $('form.create-event-form').find('.response p').html('');
                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');

                window.location.reload();
            },
            error: function(response, statut, erreur) {

                $('form.create-event-form').find('.response p').html('');
                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
            },
            complete: function(resultat, statut) {

            }
        });
    });

    $('.update_event').on('click', function(e) {
        e.preventDefault();

        var description = tmce_getContent('edit_event_description', 'edit_event_description');
        $.ajax({
            url                     : ajaxurl,
            type                    : 'POST',
            data                    : {
                action:             'update_event',
                event_name:         $('#edit_event_name').val(),
                event_description:  description,
                event_region_id:    $('#edit_tlab_region_id').val(),
                event_start_date:   $('#edit_tlab_events_startdate').val(),
                event_end_date:     $('#edit_tlab_events_enddate').val(),
                event_start_time:   $('#edit_tlab_events_starttime').val(),
                event_end_time:     $('#edit_tlab_events_endtime').val(),
                thumbnail:          $('#edit_image_attachment_id').val(),
                id:                 $('#edit_id').val(),
                status:             $('#edit_status').val(),
            },

            success: function (response, statut) {
                $('form.update-event-form').find('.response p').html('');
                $('form.update-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.update-event-form').find('.response').addClass('breadcrumb');

                window.location.reload();

                console.log(response);
            },
            error: function(response, statut, erreur) {

                $('form.create-event-form').find('.response p').html('');
                $('form.create-event-form').find('.response p').html('<i class="fa fa-info-circle"></i> '+response);
                $('form.create-event-form').find('.response').addClass('breadcrumb');
            },
            complete: function(resultat, statut) {

            }
        });
    });

    // Timepicker

    $('.tlab_timepicker .timepicker_hour').on('change', function(){

    	$(this).closest('.form-group').find('input').val($(this).val()+':'+$(this).siblings('.timepicker_minutes').val())

	});

    $('.tlab_timepicker .timepicker_minutes').on('change', function(){

        $(this).closest('.form-group').find('input').val($(this).siblings('.timepicker_hour').val()+':'+$(this).val())
    });

    $('.imageupload').imageupload({
        allowedFormats: [ 'jpg', 'png', 'gif' ],
        maxFileSizeKb: 2048
    });





    // Changement de région
    $('#dashboard .filters .tlab_region_filter').on('change', function(event){
        event.preventDefault();

        tlab_dashboard_region = $(this).val();

        $('.event-listing .panel-body > .row ').html('');
        $('.filters .fa-map-marker').hide();
        $('.filters .loading-indicator').show();



        // On charge la liste des évènenements pour cette région et le mois en cours au format JSON
        jQuery.post(
            ajaxurl,
            {
                'action': 'load_events',
                'region': tlab_dashboard_region,
                'month' : tlab_dashboard_month,
                'year'  : tlab_dashboard_year,
            },
            function(response){

                $('.event-listing .panel-body > .row ').html(response);
                $('.filters .loading-indicator').hide();
                $('.filters .fa-map-marker').show();
            }
        );

        // On implemente le template
    });

    $('#dashboard .edit_event').on('click', function(e) {
        e.preventDefault();



        // On charge la liste des évènenements pour cette région et le mois en cours au format JSON
        jQuery.post(
            ajaxurl,
            {
                'action': 'edit_event',
                'id': $(this).attr('id')
            },
            function(response){
                console.log(response);

                $('#editEvent').show();
                $('#edit_event_name').val(response.title);
                $('#edit_tlab_events_startdate').val(response.start_date);
                $('#edit_tlab_events_enddate').val(response.end_date);
                $('#edit_tlab_events_starttime').val(response.start_time);


                var start_time = response.start_time.split(':');
                var end_time = response.end_time.split(':');

                $('.start_time .timepicker_hour').val(start_time[0]);
                $('.start_time .timepicker_minutes').val(start_time[1]);
                $('.end_time .timepicker_hour').val(end_time[0]);
                $('.end_time .timepicker_minutes').val(end_time[1]);

                $('#edit_tlab_events_endtime').val(response.end_time);
                $('#edit_image-preview').attr('src', response.thumbnail);
                tmce_setContent(response.content, 'edit_event_description', 'edit_event_description');

                $('#edit_id').val(response.id);
                $('#edit_status').val(response.status);
                $('#edit_image_attachment_id').val(response.thumbnail_id);
                $('#edit_tlab_region_id').val(response.region_id);
            }
        );
    });


    $('#edit_upload_image_button').imageupload({
        allowedFormats: ['jpg', 'png', 'gif'],
        maxFileSizeKb: 2048
    });

    $('.validate_event').click(function(e){

        e.preventDefault();

        var description = tmce_getContent('edit_event_description', 'edit_event_description');
        $.ajax({
            url                     : ajaxurl,
            type                    : 'POST',
            data                    : {
                action  :    'validate_event',
                id      :    $(this).attr('id'),
            },

            success: function (response, statut) {


                window.location.reload();

            },
            error: function(response, statut, erreur) {
                    alert('Il y a eu une erreur lors de la validation. Veuillez réessayer.')
            },
            complete: function(resultat, statut) {

            }
        });
    });


});

