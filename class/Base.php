<?php

/**
 * Created by PhpStorm.
 * User: Yonel
 * Date: 30/06/2017
 * Time: 02:34
 */
class Base
{
    // Gestion des utilisateurs
    protected $user;

    protected $region;
    // Label du custom post type
    protected $cptLabel;

    public function __construct()
    {
        $this->user = new User();
    }

    // Ajout automatique des actions
    public function add_actions($actions_destination, $functions) {
        // Boucle sur le tableau
        foreach ($functions as $function) {
            // Ajout de chaque action
            add_action($actions_destination, array($this, $function));
        }
    }

    // Ajout automatique des actions
    public function register_scripts($register_destination, $scripts) {
        // Boucle sur le tableau
        foreach ($scripts as $script) {
            // Ajout de chaque action
            add_action($actions_destination, array($this, $function));
        }
    }
}