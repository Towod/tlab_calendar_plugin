<?php

/**
 * Gestion des utilisateurs
 */

class User extends Base
{
    public $currentUserId;

    // Constructeur
    public function __construct()
    {
        add_action('init', array($this, 'init'));


    }

    public function init() {
        $this->user = wp_get_current_user();
        $this->currentUserId = get_current_user_id();
        $this->currentUserRole = $this->getRoleName();
        if($this->getRoleName() == 'administrator') {
            $this->add_cap('manage_tlab_calendar_options');
        }
    }

    // Récupérer l'identifiant de la région de l'utilisateur en cours
    public function getRegionID($id = null) {



       if($this->getRoleName() == 'administrator') {
           $regionID = get_user_meta(get_current_user_id(), 'tlab_region_id', true);

       } else {
           $regionID = str_replace( array('DSC_', 'DS_', 'ELU_', 'SYN_'), '', $this->getRoleName());
       }

        return $regionID;
    }

    public function is_ADMIN() {
        return ($this->currentUserRole == 'administrator') ? true : false;
    }
    public function is_DSC() {
        return (strpos($this->currentUserRole, 'DSC_') !== false) ? true : false;
    }
    public function is_DS() {
        return (strpos($this->currentUserRole, 'DS_') !== false ) ? true : false;
    }
    public function is_ELU() {
        return (strpos($this->currentUserRole, 'ELU_') !== false ) ? true : false;
    }
    public function is_SYN() {
        return (strpos($this->currentUserRole, 'SYN_') !== false ) ? true : false;
    }

    public function getRoleName()
    {
        $userDatas = get_userdata($this->currentUserId);
        return (!empty($userDatas->roles)) ?  $userDatas->roles[0] : '';

    }

    public function add_cap($cap)
    {
        $role = get_role( $this->currentUserRole );

        $role->add_cap($cap);
    }

    public function remove_cap($cap)
    {
        $role = get_role( $this->currentUserRole );

        $role->remove_cap($cap);
    }
}