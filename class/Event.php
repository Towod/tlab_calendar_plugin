<?php

/**
 * Gestion des évènements
 */

class Event extends Base
{



    // Constructeur de l'objet
    public function __construct()
    {
        add_theme_support( 'post-thumbnails' );

        $this->user = new User();
        $this->region = new Region();
        $this->category = new Category();
        $this->calendar = new Calendar();

        $this->cptLabel = 'tlab_events';

        // Liste des fonctions à ajouter à l'initialisation
        $initActions = [
            'registerCPT', // Création du Custom Post Type
            'registerTaxonomies', // Enregistrement de la taxonomie
        ];
        $this->add_actions('init', $initActions); // Ajout des fonctions à l'initialisation

        // Shortcode de test
        add_shortcode('test_event', array($this,'create'));
        add_shortcode('create_event', array($this,'create'));
        add_shortcode('index_events', array($this,'index'));
        add_shortcode('front_index_events', array($this,'front_index'));
        add_shortcode('show_event', array($this,'show'));
        add_shortcode('draft_index', array($this,'draft_index'));
        add_shortcode('pending_index', array($this,'pending_index'));
        add_shortcode('edit_event', array($this,'edit'));

        add_action( 'wp_ajax_store_event', array($this, 'store') );
        add_action( 'wp_ajax_nopriv_store_event', array($this, 'store') );

        add_action( 'wp_ajax_destroy_event', array($this, 'destroy') );
        add_action( 'wp_ajax_nopriv_destroy_event', array($this, 'destroy') );

        add_action( 'wp_ajax_load_events', array($this, 'load') );
        add_action( 'wp_ajax_nopriv_load_events', array($this, 'load') );

        add_action( 'wp_ajax_store_to_validate', array($this, 'store_to_validate') );
        add_action( 'wp_ajax_nopriv_store_to_validate', array($this, 'store_to_validate') );

        add_action( 'wp_ajax_store_rush', array($this, 'store_rush') );
        add_action( 'wp_ajax_nopriv_store_rush', array($this, 'store_rush') );

        add_action( 'wp_ajax_load_front_events', array($this, 'load_front') );
        add_action( 'wp_ajax_nopriv_load_front_events', array($this, 'load_front') );

        add_action( 'wp_ajax_edit_event', array($this, 'edit_datas') );
        add_action( 'wp_ajax_nopriv_edit_event', array($this, 'edit_datas') );

        add_action( 'wp_ajax_update_event', array($this, 'update') );
        add_action( 'wp_ajax_nopriv_update_event', array($this, 'update') );

        add_action( 'wp_ajax_validate_event', array($this, 'pendingToPublish') );
        add_action( 'wp_ajax_nopriv_validate_event', array($this, 'pendingToPublish') );

    }

    // Enregistrement du custom post type tlab_events
    public function registerCPT()
    {
        // Définition des labels pour le PostType
        $labels = array(
            'name' => _x('Évènements', 'post type general name'),
            'singular_name' => _x('Évènement', 'post type singular name'),
            'add_new' => _x('Ajouter nouvel Évènement', $this->cptLabel),
            'add_new_item' => __('Ajouter un nouvel Évènement'),
            'edit_item' => __('Éditer l\'évènement'),
            'new_item' => __('Nouvel évènement'),
            'view_item' => __('Voir l\'évènement'),
            'search_items' => __('Chercher évènements'),
            'not_found' =>  __('Aucun évènement n\'a été trouvé'),
            'not_found_in_trash' => __('Aucun évènement n\'a été trouvé dans la corbeille'),
            'parent_item_colon' => '',
        );

        // Définition de la configuration pour le PostType
        $args = array(
            'label' => $this->cptLabel,
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'can_export' => true,
            'show_ui' => false,
            '_builtin' => false,
            'capability_type' => 'post',
            'menu_icon' => plugins_url( 'img/icons/calendar.png', dirname(__FILE__) ),
            'hierarchical' => false,
            'rewrite' => array( "slug" => "events" ),
            'supports'=> array('title', 'thumbnail', 'excerpt', 'editor') ,
            'show_in_nav_menus' => true,
            'taxonomies' => array( 'tlab_event_category', 'post_tag')
        );

        // Enregistrement du PostType
        register_post_type( $this->cptLabel, $args);
    }

    // Appel de toutes les fonctions d'enregistrement de taxonomies
    public function registerTaxonomies()
    {
        $this->registerCategoryTaxonomy();
    }

    // Enregistrement de la taxonomy Category
    public function registerCategoryTaxonomy()
    {
        // Définition des labels pour la Taxonomy
        $labels = array(
            'name' => _x( 'Catégories', 'taxonomy general name' ),
            'singular_name' => _x( 'Categorie', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Categories' ),
            'popular_items' => __( 'Popular Categories' ),
            'all_items' => __( 'All Categories' ),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __( 'Edit Category' ),
            'update_item' => __( 'Update Category' ),
            'add_new_item' => __( 'Add New Category' ),
            'new_item_name' => __( 'New Category Name' ),
            'separate_items_with_commas' => __( 'Separate categories with commas' ),
            'add_or_remove_items' => __( 'Add or remove categories' ),
            'choose_from_most_used' => __( 'Choose from the most used categories' ),
        );

        // Définition de la configuration pour la Taxonomy
        $args = array(
            'label' => __('Event Category'),
            'labels' => $labels,
            'hierarchical' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'event-category' ),
        );

        // Enregistrement de la Taxonomy
        register_taxonomy('tlab_eventcategory','tlab_events', $args);
    }

    // Affiche la liste de tous les évènements
    // Affiche la liste des évènements en foncion de $args[]
    public function index($args)
    {
        $date=new DateTime(); //this returns the current date time
        $currentDate = $date->format('m/Y');

        echo $result;

        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();



                // Récupérations des données de l'évènement
                $event_datas['title'] = get_the_title();
                $event_datas['id'] = get_the_ID();
                $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                $event_datas['permalink'] = get_the_permalink();


                if(strpos($event_datas['start_date'], '/'.$currentDate) !== false) {
                    ob_start();
                    include(dirname(__DIR__).'/templates/event/'.__FUNCTION__.'.php');
                    $result .= ob_get_clean();
                }

            endwhile;
        }

        return $result;
    }


    // Affiche la liste de tous les évènements
    // Affiche la liste des évènements en foncion de $args[]
    public function draft_index($args)
    {

        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'draft',
            'author'            => get_current_user_id(),
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $event_datas['title'] = get_the_title();
                $event_datas['id'] = get_the_ID();
                $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                $event_datas['permalink'] = get_the_permalink();
                $event_datas['status'] = 'draft';


                ob_start();
                include(dirname(__DIR__).'/templates/event/index.php');
                $result .= ob_get_clean();


            endwhile;
        }

        return $result;
    }

    // Affiche la liste de tous les évènements
    // Affiche la liste des évènements en foncion de $args[]
    public function pending_index($args)
    {

        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'pending',
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $event_datas['title'] = get_the_title();
                $event_datas['id'] = get_the_ID();
                $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                $event_datas['permalink'] = get_the_permalink();
                $event_datas['status'] = 'pending';


                ob_start();
                include(dirname(__DIR__).'/templates/event/index.php');
                $result .= ob_get_clean();


            endwhile;
        }

        return $result;
    }


    // Affiche la liste de tous les évènements
    // Affiche la liste des évènements en foncion de $args[]
    public function front_index($args)
    {
        $date=new DateTime(); //this returns the current date time
        $currentDate = $date->format('m/Y');

        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            $result .= '<div class="index_events">';
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $event_datas['title'] = get_the_title();
                $event_datas['id'] = get_the_ID();
                $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                $event_datas['permalink'] = get_the_permalink();
                $event_datas['excerpt'] = get_the_excerpt();



                if(strpos($event_datas['start_date'], '/'.$currentDate) !== false) {
                    ob_start();
                    include(dirname(__DIR__).'/templates/event/'.__FUNCTION__.'.php');
                    $result .= ob_get_clean();
                }

            endwhile;
            $result .= '</div>';
        }

        return $result;
    }

    // Affiche l'évènement $id
    public function show($atts)
    {
        $id = shortcode_atts(
            array(
                'id' => '0'
            ),
            $atts
        );

        $event = get_post($id);


        return true;

    }

    // Formulaire de création
    public function create()
    {
        wp_enqueue_media();
        add_action( 'admin_footer', array($this, 'media_selector_print_scripts') );

        ob_start();

        include_once(dirname(__DIR__).'/templates/event/'.__FUNCTION__.'.php');

        $html = ob_get_clean();

        return $html;
    }

    // Sauvegarde
    public function store()
    {

        $unvalid = $this->validate();



        if($unvalid) {


            $errors = '<ul>';
                foreach ($unvalid as $error) {
                    $errors.= '<li>'.$error.'</li>';
                }
            $errors.= '</ul>';

            header('HTTP/1.1 412 : '. $errors);

            die(false);
        }




        $name = $_POST['event_name'];

        $start_date = DateTime::createFromFormat('d/m/Y', $_POST['event_start_date']);
        $end_date = DateTime::createFromFormat('d/m/Y', $_POST['event_end_date']);

        // Create post object
        $event = array(
            'post_status'   => 'publish',
            'post_type'     => $this->cptLabel,
            'post_title'    => $name,
            'post_author'        => get_current_user_id(),
            'post_content'  => $_POST['event_description'],
            'meta_input' => array(
                'tlab_region_id' => $_POST['event_region_id'],
                'tlab_events_startdate' => $start_date->getTimestamp(),
                'tlab_events_enddate' => $end_date->getTimestamp(),
                'tlab_events_starttime' => strtotime($_POST['event_start_time']),
                'tlab_events_endtime' => strtotime($_POST['event_end_time']),
            )
        );


        $post = wp_insert_post($event);

        set_post_thumbnail( $post, $_POST['thumbnail'] );

        echo 'L\'événement a bien été enregistré';

        die();
    }

    // Sauvegarde
    public function store_to_validate()
    {

        $unvalid = $this->validate();



        if($unvalid) {


            $errors = '<ul>';
            foreach ($unvalid as $error) {
                $errors.= '<li>'.$error.'</li>';
            }
            $errors.= '</ul>';

            header('HTTP/1.1 412 : '. $errors);

            die(false);
        }




        $name = $_POST['event_name'];

        $start_date = DateTime::createFromFormat('d/m/Y', $_POST['event_start_date']);
        $end_date = DateTime::createFromFormat('d/m/Y', $_POST['event_end_date']);

        // Create post object
        $event = array(
            'post_status'   => 'pending',
            'post_type'     => $this->cptLabel,
            'post_title'    => $name,
            'post_author'        => get_current_user_id(),
            'post_content'  => $_POST['event_description'],
            'meta_input' => array(
                'tlab_region_id' => $_POST['event_region_id'],
                'tlab_events_startdate' => $start_date->getTimestamp(),
                'tlab_events_enddate' => $end_date->getTimestamp(),
                'tlab_events_starttime' => strtotime($_POST['event_start_time']),
                'tlab_events_endtime' => strtotime($_POST['event_end_time']),
            )
        );


        $post = wp_insert_post($event);

        set_post_thumbnail( $post, $_POST['thumbnail'] );

        echo 'L\'événement a bien été enregistré';

        die();
    }

    // Sauvegarde
    public function store_rush()
    {

        $unvalid = $this->validate();



        if($unvalid) {


            $errors = '<ul>';
            foreach ($unvalid as $error) {
                $errors.= '<li>'.$error.'</li>';
            }
            $errors.= '</ul>';

            header('HTTP/1.1 412 : '. $errors);

            die(false);
        }




        $name = $_POST['event_name'];

        $start_date = DateTime::createFromFormat('d/m/Y', $_POST['event_start_date']);
        $end_date = DateTime::createFromFormat('d/m/Y', $_POST['event_end_date']);

        // Create post object
        $event = array(
            'post_status'   => 'draft',
            'post_type'     => $this->cptLabel,
            'post_title'    => $name,
            'post_author'        => get_current_user_id(),
            'post_content'  => $_POST['event_description'],
            'meta_input' => array(
                'tlab_region_id' => $_POST['event_region_id'],
                'tlab_events_startdate' => $start_date->getTimestamp(),
                'tlab_events_enddate' => $end_date->getTimestamp(),
                'tlab_events_starttime' => strtotime($_POST['event_start_time']),
                'tlab_events_endtime' => strtotime($_POST['event_end_time']),
            )
        );


        $post = wp_insert_post($event);

        set_post_thumbnail( $post, $_POST['thumbnail'] );

        echo 'Le brouillon a bien été enregistré';

        die();
    }

    // Affiche le formulaire d'édition
    public function edit()
    {
            wp_enqueue_media();
            add_action( 'admin_footer', array($this, 'edit_media_selector_print_scripts') );

            ob_start();
            include_once(dirname(__DIR__).'/templates/event/'.__FUNCTION__.'.php');
            $html = ob_get_clean();

            return $html;

    }

    // Récupère l'évènement en AJAX pour l'édition
    public function edit_datas()
    {
        $id = (!empty($_POST['id'])) ? htmlspecialchars($_POST['id']) : false;

        $id = str_replace('btn-edit-event-', '', $id);

        if($id) {
            $event = get_post($id);

            $event_datas['title'] = $event->post_title;
            $event_datas['id'] = $id;
            $event_datas['start_date'] = date("j/m/Y", get_post_meta($id,'tlab_events_startdate', true));
            $event_datas['end_date'] = date("j/m/Y", get_post_meta($id, 'tlab_events_enddate', true));
            $event_datas['start_time'] = date("H:i", get_post_meta($id,'tlab_events_starttime', true));
            $event_datas['end_time'] = date("H:i", get_post_meta($id,'tlab_events_endtime', true));
            $event_datas['thumbnail'] = get_the_post_thumbnail_url($id, 'thumbnail');
            $event_datas['thumbnail_id'] = get_post_thumbnail_id($id, 'thumbnail');
            $event_datas['permalink'] = get_the_permalink($id);
            $event_datas['excerpt'] = get_the_excerpt($id);
            $event_datas['content'] = $event->post_content;
            $event_datas['status'] = $event->post_status;
            $event_datas['region_id'] = get_post_meta($id,'tlab_region_id');

            wp_send_json($event_datas);
        }

    }

    // Mise à jour de l'évènement $id avec les paramètres $args
    public function update()
    {
        $unvalid = $this->validate();



        if($unvalid) {


            $errors = '<ul>';
            foreach ($unvalid as $error) {
                $errors.= '<li>'.$error.'</li>';
            }
            $errors.= '</ul>';

            header('HTTP/1.1 412 : '. $errors);

            die(false);
        }




        $name = $_POST['event_name'];

        $start_date = DateTime::createFromFormat('d/m/Y', $_POST['event_start_date']);
        $end_date = DateTime::createFromFormat('d/m/Y', $_POST['event_end_date']);

        // Create post object
        $event = array(
            'ID'            => $_POST['id'],
            'post_status'   => $_POST['status'],
            'post_type'     => $this->cptLabel,
            'post_title'    => $name,
            'post_author'   => get_current_user_id(),
            'post_content'  => $_POST['event_description'],
            'meta_input' => array(
                'tlab_region_id' => $_POST['event_region_id'],
                'tlab_events_startdate' => $start_date->getTimestamp(),
                'tlab_events_enddate' => $end_date->getTimestamp(),
                'tlab_events_starttime' => strtotime($_POST['event_start_time']),
                'tlab_events_endtime' => strtotime($_POST['event_end_time']),
            )
        );


        $post = wp_update_post($event);

        set_post_thumbnail( $post, $_POST['thumbnail'] );

        echo 'L\'événement a bien été mis à jour';

        die();
    }

    // Index backend de la ressource
    public function destroy()
    {
        global $wpdb; // this is how you get access to the database

        $id = $_POST['event_id'];

        $post = get_post($id);

        // On valide que ce soit un évènement qui existe
        $event = $this->exist($post->post_title);

        if($event) {
            // Delete the post
            wp_delete_post( $event->ID );

            echo 'L\'évènement '.$v.' a bien été supprimé'."\r\n";

        } else {
            echo 'L\'évènement '.$id.' n\'existe pas'."\r\n";
        }


        wp_die(); // this is required to terminate immediately and return a proper response
    }

    // Indique si la région existe déja dans le custom post type tlab_regions
    public function exist( $event ) {

        if( ! empty( $event ) ) {

            $exist = get_page_by_title($event, OBJECT, $this->cptLabel);

            if($exist) {
                return $exist;
            }
        }

        return false;
    }

    // Récupérer les Évènements pour l'utilisateur courant
    function getEventsInCurrentUserRegion() {

        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $event_datas['title'] = get_the_title();
                $event_datas['start_date'] = date("F j, Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                $event_datas['end_date'] = date("F j, Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));

                foreach ($event_datas as $key => $value) {
                    $result.= $key.' => '.$value.'<br>';
                }


            endwhile;
        }

        return $result;
    }

    // Validation des données
    public function validate()
    {
        $errors = [];

        $post = $_POST;

        $inputs = [
            'event_name'        => 'required',
            'event_start_date'  => 'required',
            'event_end_date'    => 'required',
            'event_start_time'  => 'required',
            'event_end_time'    => 'required',
            'event_description' => 'required',
            'event_region_id'   => 'required',
            'thumbnail'         => '',
        ];

        foreach ($inputs as $input => $rule) {

            // Si le champ est requis mais qu'il est vide
            if(strpos($rule, 'required') !== false) {
                if(empty($post[$input])) {
                    $errors[$input] = 'Le champs '.$input.' ne peut pas être vide';
                }

            }
        }

        return (empty($errors)) ? false : $errors;
    }

    // Affiche la liste de tous les évènements
    // Affiche la liste des évènements en foncion de $args[]
    public function serialize()
    {
        $args = array(
            'post_type'         => 'tlab_events',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_key'          => 'tlab_region_id',
            'meta_query' => array(
                array(
                    'key'     => 'tlab_region_id',
                    'value'   => $this->user->getRegionID(),
                    'compare' => 'IN',
                ),
            ),
        );

        $result = [];

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $result[] = [
                    'title'         => get_the_title(),
                    'start_date'    => date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true)),
                    'end_date'      => date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true)),
                    'start_time'    => date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true)),
                    'end_time'      => date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true)),
                ];

            endwhile;
        }


        return json_encode($result);
    }

    public function media_selector_print_scripts() {

        $my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );

        ?><script type='text/javascript'>
            jQuery( document ).ready( function( $ ) {
                // Uploading files
                var file_frame;
                var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
                var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this
                jQuery('#upload_image_button').on('click', function( event ){
                    event.preventDefault();
                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        // Set the post ID to what we want
                        file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
                        // Open frame
                        file_frame.open();
                        return;
                    } else {
                        // Set the wp.media post id so the uploader grabs the ID we want when initialised
                        wp.media.model.settings.post.id = set_to_post_id;
                    }
                    // Create the media frame.
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select a image to upload',
                        button: {
                            text: 'Use this image',
                        },
                        multiple: false	// Set to true to allow multiple files to be selected
                    });
                    // When an image is selected, run a callback.
                    file_frame.on( 'select', function() {
                        // We set multiple to false so only get one image from the uploader
                        attachment = file_frame.state().get('selection').first().toJSON();
                        // Do something with attachment.id and/or attachment.url here
                        $( '#image-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
                        $( '#image_attachment_id' ).val( attachment.id );
                        // Restore the main post ID
                        wp.media.model.settings.post.id = wp_media_post_id;
                    });
                    // Finally, open the modal
                    file_frame.open();
                });
                // Restore the main ID when the add media button is pressed
                jQuery( 'a.add_media' ).on( 'click', function() {
                    wp.media.model.settings.post.id = wp_media_post_id;
                });
            });
        </script><?php
    }

    public function edit_media_selector_print_scripts() {

        $my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );

        ?><script type='text/javascript'>
            jQuery( document ).ready( function( $ ) {
                // Uploading files
                var file_frame;
                var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
                var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this
                jQuery('#edit_upload_image_button').on('click', function( event ){
                    event.preventDefault();
                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        // Set the post ID to what we want
                        file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
                        // Open frame
                        file_frame.open();
                        return;
                    } else {
                        // Set the wp.media post id so the uploader grabs the ID we want when initialised
                        wp.media.model.settings.post.id = set_to_post_id;
                    }
                    // Create the media frame.
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select a image to upload',
                        button: {
                            text: 'Use this image',
                        },
                        multiple: false	// Set to true to allow multiple files to be selected
                    });
                    // When an image is selected, run a callback.
                    file_frame.on( 'select', function() {
                        // We set multiple to false so only get one image from the uploader
                        attachment = file_frame.state().get('selection').first().toJSON();
                        // Do something with attachment.id and/or attachment.url here
                        $( '#edit_image-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
                        $( '#edit_image_attachment_id' ).val( attachment.id );
                        // Restore the main post ID
                        wp.media.model.settings.post.id = wp_media_post_id;
                    });
                    // Finally, open the modal
                    file_frame.open();
                });
                // Restore the main ID when the add media button is pressed
                jQuery( 'a.add_media' ).on( 'click', function() {
                    wp.media.model.settings.post.id = wp_media_post_id;
                });
            });
        </script><?php
    }

    // permet de charger les évènements en AJAX
    public function load()
    {
        if (!empty($_POST['region'])) {
            $args = array(
                'post_type' => 'tlab_events',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_key' => 'tlab_region_id',
                'meta_query' => array(
                    array(
                        'key' => 'tlab_region_id',
                        'value' => htmlspecialchars($_POST['region']),
                        'compare' => 'IN',
                    ),
                ),
            );

            $result = '';

            $query = new WP_Query($args);

            if ($query->have_posts()) {
                while ($query->have_posts()) : $query->the_post();

                    // Récupérations des données de l'évènement
                    $event_datas['title'] = get_the_title();
                    $event_datas['id'] = get_the_ID();
                    $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                    $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                    $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                    $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                    $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                    $event_datas['permalink'] = get_the_permalink();

                    if(strpos($event_datas['start_date'], '/'.$_POST['month'].'/'.$_POST['year']) !== false) {
                        ob_start();
                        include(dirname(__DIR__) . '/templates/event/index.php');
                        $result .= ob_get_clean();
                    }

                endwhile;
            }

            echo $result;

            wp_die();
        }

    } // permet de charger les évènements en AJAX

    public function load_front()
    {
        if (!empty($_POST['region'])) {
            $args = array(
                'post_type' => 'tlab_events',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_key' => 'tlab_region_id',
                'meta_query' => array(
                    array(
                        'key' => 'tlab_region_id',
                        'value' => htmlspecialchars($_POST['region']),
                        'compare' => 'IN',
                    ),
                ),
            );

            $result = '';

            $query = new WP_Query($args);

            if ($query->have_posts()) {
                while ($query->have_posts()) : $query->the_post();

                    // Récupérations des données de l'évènement
                    $event_datas['title'] = get_the_title();
                    $event_datas['id'] = get_the_ID();
                    $event_datas['start_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_startdate', true));
                    $event_datas['end_date'] = date("j/m/Y", get_post_meta(get_the_ID(), 'tlab_events_enddate', true));
                    $event_datas['start_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_starttime', true));
                    $event_datas['end_time'] = date("H:i", get_post_meta(get_the_ID(), 'tlab_events_endtime', true));
                    $event_datas['thumbnail'] = get_the_post_thumbnail(null, 'thumbnail');
                    $event_datas['permalink'] = get_the_permalink();
                    $event_datas['excerpt'] = get_the_excerpt();

                    if(strpos($event_datas['start_date'], '/'.$_POST['month'].'/'.$_POST['year']) !== false) {
                        ob_start();
                        include(dirname(__DIR__) . '/templates/event/front_index.php');
                        $result .= ob_get_clean();
                    }

                endwhile;
            }

            echo $result;

            wp_die();
        }

    }

    public function pendingToPublish()
    {
        $id = (!empty($_POST['id'])) ? htmlspecialchars($_POST['id']) : false;
        $id = str_replace('btn-validate-event-', '', $id);

        if($id) {
            $event = get_post($id);

            if($event) {
                $event->post_status = 'publish';

                wp_update_post($event);
            }
        }

        wp_die();
    }

}