<?php

/**
 * Created by PhpStorm.
 * User: Yonel
 * Date: 30/06/2017
 * Time: 05:21
 */
class Region extends Base
{
    private $defaultList = [
        'Actualités Nationales Publiques',
        'Actualités Nationales Privées',
        'Siège CB 21',
        'Nouvelle Aquitaine Landes Pays Basque',
        'Nouvelle Aquitaine Bordeaux',
        'Sud Ile de France',
        'Paris Seine Ouest',
        'Occitanie Midi Pyrennées',
        'Occitanie Languedoc Roussillon',
        'PACA Mougins',
        'PACA Carpentras',
        'Haut de France Picardie',
        'Haut de France Nord',
        'Grand Ouest Centre Loire',
        'Grand Ouest Normandie',
        'Grand Ouest/PDL',
        'Bourgogne Franche Comté',
        'Auvergne Rhone Alpes Ouest',
        'Auvergne Rhone Alpes Est',
        'Grand Est',
    ];

    public function __construct()
    {
        $this->cptLabel = 'tlab_regions';
        $this->user = new User();

        $this->add_actions('init', array(
            'registerCPT',
        ));

        add_shortcode('index_regions', array($this,'index'));
        add_shortcode('create_region', array($this,'create'));

        add_shortcode('regions_as_select', array($this,'displayAsSelectElement'));
        add_shortcode('regions_as_edit_select', array($this,'displayAsEditSelectElement'));
        add_shortcode('regions_as_filter', array($this,'displayAsFilter'));
        add_shortcode('regions_as_select_multiple', array($this,'displayAsSelectMultipleElement'));


        add_action( 'wp_ajax_store_region', array($this, 'store') );
        add_action( 'wp_ajax_nopriv_store_region', array($this, 'store') );

        add_action( 'wp_ajax_generate_regions', array($this, 'seed') );
        add_action( 'wp_ajax_remove_regions', array($this, 'unseed') );
    }

    // Enregistrement du custom post type tlab_events
    public function registerCPT()
    {
        // Définition des labels pour le PostType
        $labels = array(
            'name' => _x('Régions', 'post type general name'),
            'singular_name' => _x('Région', 'post type singular name'),
            'add_new' => _x('Ajouter nouvelle région', $this->cptLabel),
            'add_new_item' => __('Ajouter une nouvelle région'),
            'edit_item' => __('Éditer la région'),
            'new_item' => __('Nouvelle région'),
            'view_item' => __('Voir la région'),
            'search_items' => __('Chercher régions'),
            'not_found' =>  __('Aucune région n\'a été trouvée'),
            'not_found_in_trash' => __('Aucune région n\'a été trouvée dans la corbeille'),
            'parent_item_colon' => '',
        );

        // Définition de la configuration pour le PostType
        $args = array(
            'label' => $this->cptLabel,
            'labels' => $labels,
            'public' => true,
            'can_export' => true,
            'show_ui' => true,
            '_builtin' => false,
            'capability_type' => 'post',
            'menu_icon' => plugins_url( 'img/icons/calendar.png', dirname(__FILE__) ),
            'hierarchical' => false,
            'rewrite' => array( "slug" => "events" ),
            'supports'=> array('title') ,
            'show_in_nav_menus' => true,
        );

        // Enregistrement du PostType
        register_post_type( $this->cptLabel, $args);
    }

    // Rempli les régions par défaut
    public function seed()
    {
        foreach ($this->defaultList as $k => $v) {

            // Create post object
            $region = array(
                'post_title'    => $v,
                'post_status'   => 'publish',
                'post_type'     => 'tlab_regions'
            );

            if(!$this->exist($v)) {
                // Insert the post into the database
                $inserted = wp_insert_post( $region );
                echo 'La région '.$v.' a bien été enregistrée'."\r\n";

            } else {
                echo '# La région '.$v.' existe déjà'."\r\n";
            }


        }


        wp_die(); // this is required to terminate immediately and return a proper response
    }

    // Supprime les régions par défaut
    public function unseed()
    {
        global $wpdb; // this is how you get access to the database



        foreach ($this->defaultList as $k => $v) {

            $region = $this->exist($v);

            if($region) {
                // Insert the post into the database
                wp_delete_post( $region->ID );

                echo 'La région '.$v.' a bien été supprimée'."\r\n";

            } else {
                echo '# La région '.$v.' n\'existe pas'."\r\n";
            }


        }


        wp_die(); // this is required to terminate immediately and return a proper response
    }

    // Affiche la liste de toutes les régions
    // Affiche la liste des évènements en foncion de $args[]
    public function index($args)
    {
        $args = array(
            'post_type'         => $this->cptLabel,
            'post_statut'       => 'publish',
            'posts_per_page'    => -1,
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $region_datas['title'] = get_the_title();


                ob_start();
                include(dirname(__DIR__).'/templates/region/'.__FUNCTION__.'.php');
                $result .= ob_get_clean();


            endwhile;
        }

        return $result;
    }

    // Formulaire de création
    public function create()
    {
        ob_start();

        include_once(dirname(__DIR__).'/templates/region/'.__FUNCTION__.'.php');

        $html = ob_get_clean();

        return $html;
    }

    // Sauvegarde
    public function store()
    {
        $name = $_POST['region_name'];

        // Create post object
        $region = array(
            'post_title'    => $name,
            'post_status'   => 'publish',
            'post_type'     => $this->cptLabel,
        );

        wp_insert_post($region);

        echo 'La région a été ajoutée';

        die();
    }

    // Affiche la liste de toutes les régions dans un élément de formulaire <select>
    public function displayAsSelectElement()
    {
        $args = array(
            'post_type'         => $this->cptLabel,
            'post_statut'       => 'publish',
            'posts_per_page'    => -1,
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {

            $result .= '<select class="form-control" name="tlab_region_id" id="tlab_region_id">';

            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $region_datas['title'] = get_the_title();
                // Récupérations des données de l'évènement
                $region_datas['id'] = get_the_ID();

                if($this->user->getRegionID() == $region_datas['id']) {
                    $result .= '<option value="'.$region_datas['id'].'" selected>'.$region_datas['title'].'</option>';
                } else {
                    $result .= '<option value="'.$region_datas['id'].'" >'.$region_datas['title'].'</option>';
                }




            endwhile;

            $result .= '</select>';
        }

        return $result;
    }

    // Affiche la liste de toutes les régions dans un élément de formulaire <select>
    public function displayAsEditSelectElement()
    {
        $args = array(
            'post_type'         => $this->cptLabel,
            'post_statut'       => 'publish',
            'posts_per_page'    => -1,
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {

            $result .= '<select class="form-control" name="edit_tlab_region_id" id="edit_tlab_region_id">';

            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $region_datas['title'] = get_the_title();
                // Récupérations des données de l'évènement
                $region_datas['id'] = get_the_ID();

                if($this->user->getRegionID() == $region_datas['id']) {
                    $result .= '<option value="'.$region_datas['id'].'" selected>'.$region_datas['title'].'</option>';
                } else {
                    $result .= '<option value="'.$region_datas['id'].'" >'.$region_datas['title'].'</option>';
                }




            endwhile;

            $result .= '</select>';
        }

        return $result;
    }

    // Affiche la liste de toutes les régions dans un élément de formulaire <select>
    public function displayAsSelectMultipleElement()
    {
        $args = array(
            'post_type'         => $this->cptLabel,
            'post_statut'       => 'publish',
            'posts_per_page'    => -1,
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {

            $result .= '<select class="form-control select-picker" name="tlab_regionmultiple_id" id="tlab_regionmultiple_id" multiple>';

            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $region_datas['title'] = get_the_title();
                // Récupérations des données de l'évènement
                $region_datas['id'] = get_the_ID();

                if($this->user->getRegionID() == $region_datas['id']) {
                    $result .= '<option value="'.$region_datas['id'].'" selected>'.$region_datas['title'].'</option>';
                } else {
                    $result .= '<option value="'.$region_datas['id'].'" >'.$region_datas['title'].'</option>';
                }




            endwhile;

            $result .= '</select>';
        }

        return $result;
    }

    // Affiche la liste de toutes les régions dans un élément de formulaire <select>
    public function displayAsFilter()
    {
        $args = array(
            'post_type'         => $this->cptLabel,
            'post_statut'       => 'publish',
            'posts_per_page'    => -1,
        );

        $result = '';

        $query = new WP_Query($args);

        if ($query->have_posts()) {

            $result .= '<div class="input-group">';
            $result .= '<span class="input-group-addon" id="addon-'.$region_datas['id'].'"><i class="fa fa-map-marker"></i><img src="'.plugins_url('tlab_calendar').'/img/loader.gif" class="loading-indicator" width="13" height="13"/></span>';
            $result .= '<select class="form-control tlab_region_filter">';

            while ($query->have_posts()) : $query->the_post();

                // Récupérations des données de l'évènement
                $region_datas['title'] = get_the_title();
                // Récupérations des données de l'évènement
                $region_datas['id'] = get_the_ID();

                if($this->user->getRegionID() == $region_datas['id']) {
                    $result .= '<option value="'.$region_datas['id'].'" selected>'.$region_datas['title'].'</option>';
                } else {
                    $result .= '<option value="'.$region_datas['id'].'" >'.$region_datas['title'].'</option>';
                }




            endwhile;

            $result .= '</select>';
            $result .= '</div>';
        }

        return $result;
    }

    // Indique si la région existe déja dans le custom post type tlab_regions
    public function exist( $region ) {

        if( ! empty( $region ) ) {

            $exist = get_page_by_title($region, OBJECT, $this->cptLabel);

            if($exist) {
                return $exist;
            }
        }

        return false;
    }
}